#[macro_use]
extern crate diesel;
extern crate tor_file_parser;

mod models;
mod schema;

use std::env;
use std::error::Error;
use std::fmt::{self, Debug, Display, Formatter};
use std::fs::{File, OpenOptions, remove_dir_all, remove_file};
use std::io::{self, Read, Write};
use std::process::Command;
use std::str;

use diesel::{
    Connection,
    ConnectionError,
    ExpressionMethods,
    QueryDsl,
    RunQueryDsl,
    select,
    SqliteConnection
};
use diesel::dsl::exists;
use tor_file_parser::DescIter;

pub use models::{Router, RouterDbError};

const UP_SQL: &'static str =
    "CREATE TABLE routers (\n\
        \tfingerprint VARCHAR PRIMARY KEY NOT NULL,\n\
        \tnick VARCHAR NOT NULL,\n\
        \tflags VARCHAR,\n\
        \tbandwidth BigInt NOT NULL\n\
    )";

const DOWN_SQL: &'static str = "DROP TABLE routers";

fn write_up_file(up_path: &str) -> io::Result<()> {
    let mut file = OpenOptions::new().write(true).open(up_path)?;
    let bytes = UP_SQL.as_bytes();
    file.write(bytes).map(|_| ())
}

fn write_down_file(down_path: &str) -> io::Result<()> {
    let mut file = OpenOptions::new().write(true).open(down_path)?;
    let bytes = DOWN_SQL.as_bytes();
    file.write(bytes).map(|_| ())
}

fn create_db_and_migrations_dir(db_path: &str) -> io::Result<()> {
    Command::new("diesel")
        .args(&["setup", "--database-url", db_path])
        .output().map(|_| ())
}

fn create_routers_table(db_path: &str) -> io::Result<()> {
    let output = Command::new("diesel")
        .args(&["migration", "generate", "create_table"])
        .output()?;

    let lines: Vec<_> = str::from_utf8(&output.stdout).unwrap()
        .lines()
        .collect();

    let up_path = lines[0].trim_left_matches("Creating ").to_string();
    let down_path = lines[1].trim_left_matches("Creating ").to_string();

    let _ = write_up_file(&up_path)?;
    let _ = write_down_file(&down_path)?;

    Command::new("diesel")
        .args(&["migration", "run", "--database-url", db_path])
        .output().map(|_| ())
}

pub struct RouterDb {
    db_path: String,
    delete_on_drop: bool,
    conn: SqliteConnection,
    log_file: Option<File>
}

impl RouterDb {
    /// Creates a SQLite database and a "routers" table, then establishes
    /// a connection to the database. In this constructor (as opposed to
    /// `RouterDB::create_temporary`), the created database and migrations
    /// are persistant (ie. not deleted when the RouterDb instance goes out
    /// of scope).
    pub fn create(db_name: &str) -> Result<Self, Box<Error + 'static>> {
        let cwd = env::var("CARGO_MANIFEST_DIR").unwrap();
        let db_path = format!("{}/{}", cwd, db_name);

        let _ = create_db_and_migrations_dir(&db_path)?;
        let _ = create_routers_table(&db_path)?;

        RouterDb::connect(&db_path).map_err(|e| From::from(e))
    }

    /// Creates a SQLite database and a "routers" table, then establishes
    /// a connection to the database. This constructor (as opposed to
    /// `RouterDB::create`), deletes the created database and migrations
    /// folder when the RouterDb instance goes out of scope.
    pub fn create_temporary(db_name: &str) -> Result<Self, Box<Error + 'static>> {
        let mut db = RouterDb::create(db_name)?;
        db.delete_on_drop = true;
        Ok(db)
    }

    /// Instantiates an instance of RouterDb around an existing SQLite
    /// database and "routers" table.
    pub fn connect(db_path: &str) -> Result<Self, ConnectionError> {
        let db = RouterDb {
            db_path: db_path.to_string(),
            delete_on_drop: false,
            conn: SqliteConnection::establish(db_path)?,
            log_file: None
        };
        Ok(db)
    }

    /// Enable logging to a ".log" in the current working directory
    /// (directory containing Cargo.toml). If a ".log" file already exists,
    /// it will be overwritten.
    pub fn dotlog(&mut self) {
        let _ = remove_file(".log");
        self.log_file = Some(File::open(".log").unwrap());
    }

    /// Counts the number of routers in the databse.
    pub fn n_routers(&self) -> i64 {
        use schema::routers::dsl::*;
        routers.count().get_result(&self.conn).unwrap()
    }

    /// Checks if the database is empty.
    pub fn is_empty(&self) -> bool {
        self.n_routers() == 0
    }

    /// Returns a vector of all rows in the database.
    pub fn routers(&self) -> Vec<Router> {
        use schema::routers;
        routers::table.load::<Router>(&self.conn).unwrap()
    }

    /// Insert a Router into the database.
    pub fn insert(&self, router: &Router) {
        use schema::routers::dsl::routers;
        let _ = diesel::insert_into(routers)
            .values(router)
            .execute(&self.conn).unwrap();
    }

    /// Parse descriptors from a reader and insert into the the database.
    pub fn insert_descriptors<R: Read>(&mut self, readable: R) {
        for desc in DescIter::from_reader(readable) {
            match Router::from_desc(desc) {
                Ok(row) => self.insert(&row),
                Err(error) => self.log_error(error)
            }
        }
    }

    /// Checks if there exists a row whose primary key is `fp`.
    pub fn contains_fingerprint(&self, fp: &str) -> bool {
        use schema::routers::dsl::{fingerprint, routers};
        select(exists(routers.filter(fingerprint.eq(fp))))
            .get_result(&self.conn)
            .unwrap_or(false)
    }

    /// Counts the number of routers whose fingerpint is `fp`. Ideally
    /// this is only ever zero or one.
    pub fn n_routers_with_fingerprint(&self, fp: &str) -> i64 {
        use schema::routers::dsl::{fingerprint, routers};
        routers.filter(fingerprint.eq(fp))
            .count()
            .get_result(&self.conn)
            .unwrap()
    }

    /// Returns all routers in ascending order by bandwidth.
    pub fn order_bandwidth_asc(&mut self) -> Vec<Router> {
        use schema::routers::dsl::{bandwidth, routers};
        routers.order(bandwidth.asc())
            .load::<Router>(&self.conn).unwrap()
    }

    fn log_error<T: Debug>(&mut self, error: T) {
        if let Some(ref mut log_file) = self.log_file {
            let msg = format!("[error] {:?}", error);
            let _ = log_file.write(msg.as_bytes());
        }
    }
}

impl Drop for RouterDb {
    fn drop(&mut self) {
        if self.delete_on_drop {
            let cwd = env::var("CARGO_MANIFEST_DIR").unwrap();
            let migrations_dir = format!("{}/migrations", cwd);
            let _ = remove_file(&self.db_path);
            let _ = remove_dir_all(&migrations_dir);
        }
    }
}

impl Display for RouterDb {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let s = format!(
            "RouterDb {{\n\
                \tdb_path: {},\n\
                \tdelete_on_drop: {},\n\
                \tconn: SqliteConnection\n\
            }}",
            self.db_path,
            self.delete_on_drop
        );
        write!(f, "{}", s)
    }
}

impl Debug for RouterDb {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

#[test]
fn test_router_db() {
    let res = RouterDb::create_temporary("router.db");
    assert!(res.is_ok());
}

#[test]
fn test_insert() {
    let db = RouterDb::create_temporary("router.db").unwrap();
    assert!(db.is_empty());
    db.insert(&Router::default());
    assert_eq!(db.n_routers(), 1);
}

#[test]
fn test_contains_fingerprint() {
    let db = RouterDb::create_temporary("router.db").unwrap();
    let router = Router::default();
    assert!(!db.contains_fingerprint(&router.fingerprint));
    db.insert(&router);
    assert!(db.contains_fingerprint(&router.fingerprint));
}

#[test]
fn test_n_routers_with_fingerprint() {
    let db = RouterDb::create_temporary("router.db").unwrap();
    let router = Router::default();
    assert_eq!(db.n_routers_with_fingerprint(&router.fingerprint), 0);
    db.insert(&router);
    assert_eq!(db.n_routers_with_fingerprint(&router.fingerprint), 1);
}

#[test]
fn test_insert_descriptors() {
    let bytes =
        "@type bridge-network-status 1.2\n\
        r one AFcEo7p09hkP3jmnRwWj8U3yj08 HW4VswH31fxbT48Hi6aDyaN0ggs 2018-01-30 11:26:29 10.10.10.10 12345 0\n\
        w Bandwidth=100\n\
        r two ADXqKmHijTlfCArKIkRTlJDnCVA fMqxc0gYAIlx63wXXQXCRi0n/us 2018-02-07 18:46:45 10.141.55.204 61282 0\n\
        w Bandwidth=200".as_bytes();

    let mut db = RouterDb::create_temporary("router.db").unwrap();
    db.insert_descriptors(bytes);
    assert_eq!(db.n_routers(), 2);
}

#[test]
fn test_order_bandwidth_asc() {
    let mut db = RouterDb::create_temporary("router.db").unwrap();

    let routers = vec![
        Router {
            fingerprint: "a".to_string(),
            bandwidth: 3,
            ..Default::default()
        },
        Router {
            fingerprint: "b".to_string(),
            bandwidth: 1,
            ..Default::default()
        },
        Router {
            fingerprint: "c".to_string(),
            bandwidth: 2,
            ..Default::default()
        }
    ];

    for router in routers {
        db.insert(&router);
    }

    let ordered = db.order_bandwidth_asc();
    assert_eq!(ordered[0].bandwidth, 1);
    assert_eq!(ordered[1].bandwidth, 2);
    assert_eq!(ordered[2].bandwidth, 3);
}
