table! {
    routers (fingerprint) {
        fingerprint -> Text,
        nick -> Text,
        flags -> Nullable<Text>,
        bandwidth -> BigInt,
    }
}
