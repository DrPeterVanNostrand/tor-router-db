use super::schema::routers;

use tor_file_parser::Descriptor;

#[derive(Debug)]
pub enum RouterDbError {
    DescriptorMissingFingerprint,
    DescriptorMissingNick,
    DescriptorMissingBandwidth
}

#[derive(Clone, Debug, Default, Insertable, Queryable)]
#[table_name="routers"]
pub struct Router {
    pub fingerprint: String,
    pub nick: String,
    pub flags: Option<String>,
    pub bandwidth: i64
}

impl Router {
    pub fn from_desc(desc: Descriptor) -> Result<Self, RouterDbError> {
        let fingerprint = desc.get_fingerprint()
            .ok_or(RouterDbError::DescriptorMissingFingerprint)?;

        let nick = desc.get_nick()
            .ok_or(RouterDbError::DescriptorMissingNick)?;

        let flags = desc.s.map(|flags|
            flags.iter()
                .map(|flag| flag.as_str())
                .collect::<Vec<_>>()
                .join(",")
        );

        let bandwidth = if let Some(w) = desc.w {
            w.bandwidth as i64
        } else {
            return Err(RouterDbError::DescriptorMissingBandwidth);
        };

        let router = Router { fingerprint, nick, flags, bandwidth };
        Ok(router)
    }
}

#[test]
fn test_router_from_desc() {
    let desc_bytes =
        "r nikolai AFcEo7p09hkP3jmnRwWj8U3yj08 HW4VswH31fxbT48Hi6aDyaN0ggs 2018-01-30 11:26:29 10.10.10.10 12345 0\n\
        s Running Stable V2Dir Valid\n\
        w Bandwidth=1000 Measured=500 Unmeasured=1".as_bytes();

    let desc = Descriptor::new(desc_bytes);
    assert!(Router::from_desc(desc).is_ok());
}
