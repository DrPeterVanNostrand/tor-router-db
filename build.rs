use std::process::{Command, Stdio};

fn is_sqlite_installed() -> bool {
    let proc_res = Command::new("sqlite3")
        .arg("--version")
        .stdout(Stdio::null())
        .spawn();

    if let Ok(mut child) = proc_res {
        if let Ok(_) = child.wait() {
            return true;
        }
    }
    false
}

fn is_diesel_cli_installed() -> bool {
    let proc_res = Command::new("diesel")
        .arg("--version")
        .stdout(Stdio::null())
        .spawn();

    if let Ok(mut child) = proc_res {
        if let Ok(_) = child.wait() {
            return true;
        }
    }
    false
}

fn main() {
    if !is_diesel_cli_installed() {
        panic!("Diesel CLI is not installed.");
    }

    if !is_sqlite_installed() {
        panic!("SQLite3 is not installed.");
    }
}
