# tor-router-db

A SQLite database for Tor descriptors.

# About

This crate provides persistant storage for Tor descriptors parsed using the
[`tor-file-parser` crate](https://gitlab.com/DrPeterVanNostrand/tor-file-parser).

# Installation

To use this crate from a project of your own, add the following to your
project's `Cargo.toml`:

    [dependencies]
    tor-router-db = { git = "https://gitlab.com/DrPeterVanNostrand/tor-router-db" }

# Testing

As some tests do filesystem manipulations (creating and removing SQLite
migration files), `cargo test` must be run in single-threaded mode.

    $ cargo test -- --test-threads=1

# Usage

```rust
extern crate tor_file_parser;
extern crate tor_router_db;

use std::convert::TryFrom;

use tor_file_parser::DescIter;
use tor_router_db::{Router, RouterDb};

fn main() {
    let db = RouterDb::create_temporary("router.db").unwrap();

    for desc in DescIter::new("server-descriptors") {
        if let Ok(router) = Router::try_from(desc) {
            db.insert(&router);
        }
    }

    println!("N ROUTERS => {}", db.n_routers());
}
```

# Contributing

Details on how to contribute can be found in `CONTRIBUTING.md`.

